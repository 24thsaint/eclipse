/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   newmain.cpp
 * Author: apple-ad2c437ed0361ed0dee5a47534
 *
 * Created on January 24, 2017, 9:46 AM
 */

#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>

using namespace std;

void initialize();
void gameLoop();
void destroy();
void moveBall();
void displayStats();

ALLEGRO_DISPLAY *display; // THE WINDOW ITSELF
ALLEGRO_EVENT_QUEUE *event_queue; // Remembers and triggers events
ALLEGRO_TIMER *timer; // In charge of refreshing view
const float FPS = 60;
bool redraw = false;

ALLEGRO_COLOR color;
ALLEGRO_COLOR black;
ALLEGRO_COLOR white;
ALLEGRO_COLOR textColor;

ALLEGRO_FONT *font;
int mouseX = 0;
int mouseY = 0;

int ballX = 250;
int ballY = 250;
int ballSize = 30;
int moveSpeed = 5;

int movementX = -1;
int movementY = -1;

int screenHeight = 500;
int screenWidth = 500;

int start = true;

int click = 0;
bool gameHasEnded = false;
int centerPositionX = 250;
int centerPositionY = 250;

/*
 * 
 */
int main(int argc, char** argv) {
    initialize();
    gameLoop();
    destroy();
    return EXIT_SUCCESS;
}

void initialize() {
    al_init(); // initialize allegro
    al_init_font_addon(); // initialize font add-on
    al_init_ttf_addon(); // initialize ttf add-on for .tff fonts
    al_install_mouse(); // initialize mouse input
    al_init_primitives_addon(); // install graphics add-on
    // tell allegro to display a resizable window
    al_set_new_display_flags(ALLEGRO_RESIZABLE);
    // create and show the display window
    display = al_create_display(screenWidth, screenHeight);
    al_set_window_title(display, "DEMONSTRATION");
    /*
     * Loads the font.
     * The font file is located in the project directory.
     * Params (font name, size, flags)
     */
    font = al_load_ttf_font("fixed_font.ttf", 20, 0);
    // set background color of window
    al_clear_to_color(al_map_rgb(0, 0, 0));
    // ******* Register events
    event_queue = al_create_event_queue();
    timer = al_create_timer(1.0 / FPS);
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_mouse_event_source());
    // ***********************
    // I think this updates the display stack
    al_flip_display();
    al_start_timer(timer);

    black = al_map_rgb(0, 0, 0);
    white = al_map_rgb(255, 255, 255);
    textColor = white;
}

void gameLoop() {
    while (true) {
        ALLEGRO_EVENT ev;

        al_wait_for_event(event_queue, &ev);

        if (ev.type == ALLEGRO_EVENT_TIMER) {
            redraw = true;
        } else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            break;
        } else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
            color = white;
            textColor = black;

            if (start) {
                click++;
            }
            
            if (gameHasEnded) {
                click = 0;
                gameHasEnded = false;
            }
        } else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
            color = black;
            textColor = white;
            start = !start;
        } else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES) {
            mouseX = ev.mouse.x;
            mouseY = ev.mouse.y;
        }

        if (redraw && al_is_event_queue_empty(event_queue)) {
            redraw = false;
            // ********** MAJOR LOGIC
            al_clear_to_color(color);
            al_draw_textf(font, textColor, 0, 0, ALLEGRO_ALIGN_LEFT, "Click: %d", click);
            al_draw_filled_circle(centerPositionX, centerPositionY, ballSize, white);

            if (ballX == centerPositionX && !start) {
                al_draw_textf(font, textColor, 250, 150, ALLEGRO_ALIGN_CENTER, "CONGRATULATIONS YOU'VE GOT %d CLICKS", click);
                gameHasEnded = true;
            }
            moveBall();
            // **********************
            al_flip_display();
        }
    }
}

void moveBall() {
    if (ballX <= ballSize) {
        movementX *= -1;
    } else if (ballX >= screenHeight - ballSize) {
        movementX *= -1;
    }

    if (start) {
        moveSpeed += 0.1;
        ballX += movementX * moveSpeed;
    }

    al_draw_filled_circle(ballX, ballY, ballSize, al_map_rgb(255, 0, 0));
}

void destroy() {
    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(event_queue);
}